FROM python:3.9

# Directory setup
RUN mkdir /src
WORKDIR /src

COPY ./oTree/ /src/

RUN apt-get install libpq-dev

RUN pip install -r requirements.txt

EXPOSE 8000

CMD [ "otree", "prodserver", "8000"] 