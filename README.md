# oForest

You will find the detailed docs here [devops.jasperanders.xyz](https://devops.jasperanders.xyz).

## New Experiment Setup for Experimenter

1. Clone the oGardener and create a new project in the oForest group. Add this new repo as origin. To do this, make sure you have Git installed and run:
   ```bash
   # rename folder for your project
   git mv oGardener YOUR_PROJECT_NAME
   # rename old origin to oGardener
   git remote rename origin oGardener
   # add your project url
   git remote add origin YOUR_PROJECT_URL
   ```

2. Move your experiment code to oTree folder. Also make sure to include all dependencies in your `requirements.txt` or `Pipenv & Pipenv.lock`. If you have missing dependencies, your project won't run.

3. Make sure to leave `.resetdb.sh` in the oTree folder untouched. It is a hidden file, if you don't see it, don't worry about it.

4. Change ingress URL according to your assigned project URL. For this, add an environment variable to your GitLab project. Name it EXPERIMENT_URL it should look something like this: `experiment01.k2lab.iism.kit.edu`

5. Next you must add the following CI/CD variables (Settings > CI/CD > Variables)
   - `MASTER_PWD` choose a good password. This is the password you will use to access your deployment. It should be masked and not protected.
   - `PROJECT_URL` this is the url assigned to you by your administator.
   - `TLS_SECRET_NAME` this is your project id. If you project is located at `experiment01.k2lab.iism.kit.edu` the secretname should be `experiment01`.
   
6. If necessary, adjust the python version in the `Dockerfile` to match the oTree version you're using.

7. Stage, commit and push your changes. Make sure you push to `origin` not to `oGardener`. You can watch the pipeline running in GitLab, wait for it to be finished.

8. Now manually deploy to Production. Here you can also reset your database or delete the whole deployment.
