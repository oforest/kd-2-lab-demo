from otree.api import *
import pandas as pd

doc = """
Your app description
"""


class C(BaseConstants):
    NAME_IN_URL = 'graphs'
    PLAYERS_PER_GROUP = None
    NUM_ROUNDS = 1


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    competition = models.BooleanField()
    effort = models.IntegerField()
    cheats = models.IntegerField()
    contribution = models.CurrencyField()


# PAGES
class MyPage(WaitPage):

    @staticmethod
    def after_all_players_arrive(group: Group):
        ps = group.get_players()
        for p in ps:
            part = p.participant
            p.competition = part.vars['competition']
            p.effort = part.vars['effort']
            p.cheats = part.vars['cheats']
            p.contribution = part.vars['contribution']


class ResultsWaitPage(WaitPage):
    pass


class Results(Page):

    @staticmethod
    def js_vars(player: Player):
        group = player.group
        player_effort_comp = []
        player_effort_base = []
        player_contribution_base = []
        player_contribution_comp = []
        player_cheats_base = []
        player_cheats_comp = []
        for p in group.get_players():
            if p.competition == 1:
                player_effort_comp.append(p.effort)
                player_contribution_comp.append(p.contribution)
                player_cheats_comp.append(p.cheats)
            else:
                player_effort_base.append(p.effort)
                player_contribution_base.append(p.contribution)
                player_cheats_base.append(p.cheats)

        bin_effort_labels_b = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
                               '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25']
        binned_effort_data_b = pd.cut(player_effort_base,
                                      [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                                       21, 22, 23, 24, 25],
                                      include_lowest=True, labels=bin_effort_labels_b)
        binned_effort_sizes_b = binned_effort_data_b.value_counts().sort_index()
        print(len(player_effort_base))

        bin_effort_labels_c = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
                               '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25']
        binned_effort_data_c = pd.cut(player_effort_comp,
                                      [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                                       21, 22, 23, 24, 25],
                                      include_lowest=True, labels=bin_effort_labels_c)
        binned_effort_sizes_c = binned_effort_data_c.value_counts().sort_index()

        bin_cheat_labels_b = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
                              '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25']
        binned_cheat_data_b = pd.cut(player_cheats_base,
                                     [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                                      21, 22, 23, 24, 25],
                                     include_lowest=True, labels=bin_cheat_labels_b)
        binned_cheat_sizes_b = binned_cheat_data_b.value_counts().sort_index()

        bin_cheat_labels_c = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
                              '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25']
        binned_cheat_data_c = pd.cut(player_cheats_comp,
                                     [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                                      21, 22, 23, 24, 25],
                                     include_lowest=True, labels=bin_cheat_labels_c)
        binned_cheat_sizes_c = binned_cheat_data_c.value_counts().sort_index()

        bin_contribution_labels_b = ['0-10', '10-20', '20-30', '30-40', '40-50', '50-60', '60-70', '70-80', '80-90',
                                     '90-100']
        binned_contribution_data_b = pd.cut(player_contribution_base,
                                     [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
                                     include_lowest=True, labels=bin_contribution_labels_b)
        binned_contribution_sizes_b = binned_contribution_data_b.value_counts().sort_index()

        bin_contribution_labels_c = ['0-10', '10-20', '20-30', '30-40', '40-50', '50-60', '60-70', '70-80', '80-90',
                                     '90-100']
        binned_contribution_data_c = pd.cut(player_contribution_comp,
                                     [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
                                     include_lowest=True, labels=bin_contribution_labels_c)
        binned_contribution_sizes_c = binned_contribution_data_c.value_counts().sort_index()

        return dict(
            total_players_effort_base=len(player_effort_base),
            bin_labels_effort_base=bin_effort_labels_b,
            player_bins_effort_base=binned_effort_sizes_b.to_json(),
            total_players_effort_comp=len(player_effort_comp),
            bin_labels_effort_comp=bin_effort_labels_c,
            player_bins_effort_comp=binned_effort_sizes_c.to_json(),
            total_players_cheat_base=len(player_cheats_base),
            bin_labels_cheat_base=bin_cheat_labels_b,
            player_bins_cheat_base=binned_cheat_sizes_b.to_json(),
            total_players_cheat_comp=len(player_cheats_comp),
            bin_labels_cheat_comp=bin_cheat_labels_c,
            player_bins_cheat_comp=binned_cheat_sizes_c.to_json(),
            total_players_contribution_base=len(player_contribution_base),
            bin_labels_contribution_base=bin_contribution_labels_b,
            player_bins_contribution_base=binned_contribution_sizes_b.to_json(),
            total_players_contribution_comp=len(player_contribution_comp),
            bin_labels_contribution_comp=bin_contribution_labels_c,
            player_bins_contribution_comp=binned_contribution_sizes_c.to_json()
        )


page_sequence = [MyPage, ResultsWaitPage, Results]
