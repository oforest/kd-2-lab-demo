from otree.api import *


doc = """
Your app description
"""


class C(BaseConstants):
    NAME_IN_URL = 'instructions'
    PLAYERS_PER_GROUP = None
    NUM_ROUNDS = 1


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass


class Player(BasePlayer):

    competition = models.BooleanField()


def creating_session(subsession: Subsession):
    import random
    t = 0
    f = 0
    for player in subsession.get_players():
        if t < subsession.session.num_participants/2:
            if f < subsession.session.num_participants/2:
                player.competition = random.choice([True, False])
            else:
                player.competition = True
        else:
            player.competition = False
        if player.competition:
            t += 1
        else:
            f += 1
        print('set competition to', player.competition)


# PAGES
class MyPage(Page):

    @staticmethod
    def before_next_page(player: Player, timeout_happened):
        p = player.participant
        p.competition = player.competition


page_sequence = [MyPage]
