from otree.api import *


class C(BaseConstants):
    NAME_IN_URL = 'public_goods_simple'
    PLAYERS_PER_GROUP = None
    NUM_ROUNDS = 1
    ENDOWMENT = cu(100)
    MULTIPLIER = 1.8


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    contribution = models.CurrencyField(
        min=0, max=C.ENDOWMENT, label="Wie viel wollen Sie zum Projekt beitragen?"
    )
    competition = models.BooleanField()

    total_contribution = models.CurrencyField()
    individual_share = models.CurrencyField()


# FUNCTIONS

# PAGES
class Instructions(Page):

    @staticmethod
    def vars_for_template(player: Player):
        ps = player.get_others_in_group()
        c = 1
        for p in ps:
            c += 1
        return dict(c=int(c / 2))

    @staticmethod
    def before_next_page(player: Player, timeout_happened):
        player.competition = player.participant.competition


class Contribute(Page):
    form_model = 'player'
    form_fields = ['contribution']

    @staticmethod
    def vars_for_template(player: Player):
        ps = player.get_others_in_group()
        c = 1
        for p in ps:
            c += 1
        return dict(c=c/2)


class ResultsWaitPage(WaitPage):

    @staticmethod
    def after_all_players_arrive(group: Group):
        players = group.get_players()
        for p in players:
            n_players = 1
            contributions = 0
            p2 = p.get_others_in_subsession()
            for q in p2:
                if q.competition == p.competition:
                    n_players += 1
                    contributions = contributions + q.contribution
            p.total_contribution = contributions + p.contribution
            p.individual_share = (
                    p.total_contribution * C.MULTIPLIER / n_players
            )
        for p in players:
            p.payoff = C.ENDOWMENT - p.contribution + p.individual_share


class Results(Page):

    @staticmethod
    def vars_for_template(player: Player):
        ps = player.get_others_in_group()
        c = 1
        for p in ps:
            c += 1
        return dict(c=c / 2)

    @staticmethod
    def before_next_page(player: Player, timeout_happened):
        part = player.participant
        part.vars['contribution'] = player.contribution


page_sequence = [Instructions, Contribute, ResultsWaitPage, Results]
